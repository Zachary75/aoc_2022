import java.io.File

fun main(args: Array<String>) {
    val preinput = File("C:\\Users\\zacha\\IdeaProjects\\AdventOfCode_2022\\src\\main\\resources\\input").readText()
    val input = preinput.trim()
    val message = input.toList()
    var index = 13
    var found = false

   /* while (!found){
        var str1 = message[index]
        var str2 = message[index-1]
        var str3 = message[index-2]
        var str4 = message[index-3]

        found = compare(str1,str2,str3,str4)
        if(!found){
            index++
        }
    }*/
    while(!found){
        val set = message.subList(index-13,index+1).toSet()
        val size = set.size
        if(size==14){found=true}
        if(!found){index++}
    }

    println(index+1)
}

fun compare(a:Char,b:Char,c:Char,d:Char) : Boolean{
    if(a!=b&&a!=c&&a!=d&&b!=c&&b!=d&&c!=d){return true}
    return false
}