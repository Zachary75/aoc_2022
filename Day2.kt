import java.io.File
// a= rock b= pap c=scissor
// y=pap z=scissor x=rock

//x>b
//y>a
//z>c
fun main(args: Array<String>){
    val preinput = File( "C:\\Users\\zacha\\IdeaProjects\\AdventOfCode_2022\\src\\main\\resources\\input" ).readText()
    val input = preinput.trim().split("\r\n")

    var finalscore = 0
   for(s in input){
        val sus = s.trim()
        finalscore += rockPaperScissorsV2(sus)
    }
    println(finalscore)
}

//deprecated because cringe
fun rockPaperScissorsV1(s: String) :Int{
    var me = s.substring(2,3)
    var him = s.substring(0,1)

    var myscore = 0

    if(me == "X") {
        me = "rock"
        myscore +=1 }
    else if (me == "Y"){
        me = "pap"
        myscore +=2 }
    else if (me == "Z"){
        me = "scis"
        myscore +=3 }

    if(him == "A") {
        him = "rock"}
    else if (him == "B"){
        him = "pap"}
    else if (him == "C"){
        him = "scis"
    }

    if(me == him){
        myscore +=3
    }
    else if (me == "rock" && him == "scis"){
         myscore +=6
    }
    else if (me == "pap" && him == "rock"){
        myscore +=6
    }
    else if (me == "scis" && him == "pap"){
        myscore +=6
    }
    else if (me == "scis" && him == "rock"){
        myscore +=0
    }
    else if (me == "rock" && him == "pap"){
        myscore +=0
    }
    else if (me == "pap" && him == "scis"){
        myscore +=0
    }


    return myscore
}

fun rockPaperScissorsV2(s: String) :Int{
    var me = s.substring(2,3)
    var him = s.substring(0,1)

    var myscore = 0

    if(me == "X") {
        me = "lose"
        }
    else if (me == "Y"){
        me = "draw"
         }
    else if (me == "Z"){
        me = "win"
         }

    if(him == "A") {
        him = "rock"}
    else if (him == "B"){
        him = "pap"}
    else if (him == "C"){
        him = "scis"
    }

    if(me == "draw"){
        me = him
    }
    else if(me == "lose") {
        if (him == "rock") {
            me = "scis"
        } else if (him == "pap") {
            me = "rock"
        } else if (him == "scis") {
            me = "pap"
        }
    }
    else if(me =="win"){
        if (him == "rock") {
            me = "pap"
        } else if (him == "pap") {
            me = "scis"
        } else if (him == "scis") {
            me = "rock"
        }
    }

    if(me == "rock") {
        myscore +=1 }
    else if (me == "pap"){
        myscore +=2 }
    else if (me == "scis"){
        myscore +=3 }

    if(me == him){
        myscore +=3
    }
    else if (me == "rock" && him == "scis"){
        myscore +=6
    }
    else if (me == "pap" && him == "rock"){
        myscore +=6
    }
    else if (me == "scis" && him == "pap"){
        myscore +=6
    }



    return myscore
}