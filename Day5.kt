import java.io.File
import java.lang.RuntimeException

fun main(args: Array<String>) {
    val preinput = File("C:\\Users\\zacha\\IdeaProjects\\AdventOfCode_2022\\src\\main\\resources\\input").readText()
    val input = preinput.trim().split("\r\n")
    val preinput2 = File("C:\\Users\\zacha\\IdeaProjects\\AdventOfCode_2022\\src\\main\\resources\\input2").readText()
    val input2 = preinput2.trim().split("\r\n")
    var stacks = input2.toMutableList()

    var output = ""
    var ignore = 0
    //move 1 from 3 to 5
    for(s in input){
        if(ignore<10){ignore++}
        else{
            val k = s.split(" ")

            var count: Int = k[1].toInt()
            var count2:Int = 0
            val moveFrom = k[3].trim().toInt()-1
            val moveTo = k[5].trim().toInt()-1
            println(s)
            val moveString = stacks[moveFrom]
            if(!(moveString=="")) {
                if (moveString.length - count <= -1) {
                    count = 0
                } else {
                     count2 = moveString.length - count
                }
                stacks[moveTo] = stacks[moveTo] + moveString.substring(count2)
                var moveFromNewString = moveString.substring( 0, (moveString.length - count))
                stacks[moveFrom] = moveFromNewString
                var l = 0
            }
        }
    }

    println(stacks)
}

fun reverseString(s: String) :String{
    var newString = ""
    for(k in s){
        newString = k + newString
    }
    return newString
}