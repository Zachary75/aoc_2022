import java.io.File

fun main(args: Array<String>) {
    val preinput = File( "C:\\Users\\zacha\\IdeaProjects\\AdventOfCode_2022\\src\\main\\resources\\input" ).readText()
    val input = preinput.trim().split("\r\n")

    var us = 0
    var max1 = 0
    var max2 = 0
    var max3 = 0
    for(s in input) {
        val sus =s.trim()
        if (sus == "") {
            us = 0
        } else {
            us += sus.toInt()
        }
        //new after this
        if (us>max1){
            max3 =max2
            max2 =max1
            max1 = us
        }
        else if (us>max2){
            max3 =max2
            max2 = us
        }
        else if (us>max3){
            max3 =us
        }
    }
    println(max1)
    println(max2)
    println(max3)
    println("")
    println(max1+max2+max3)
}

