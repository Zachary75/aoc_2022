import java.io.File

fun main(args: Array<String>) {
    val preinput = File( "C:\\Users\\zacha\\IdeaProjects\\AdventOfCode_2022\\src\\main\\resources\\input" ).readText()
    val input = preinput.trim().split("\r\n")
    var output1 = 0
    var output2 = 0
    for (a in input){
        var aComma = a.indexOf(",")


        var aTask1 = a.substring(0,aComma)
        var aTask2 = a.substring(aComma+1)

        var a11 = aTask1.substring(0,aTask1.indexOf("-")).toInt()
        var a12 = aTask1.substring(aTask1.indexOf("-")+1).toInt()
        var a21 = aTask2.substring(0,aTask2.indexOf("-")).toInt()
        var a22 = aTask2.substring(aTask2.indexOf("-")+1).toInt()


        if(a11 <= a21 && a12 >= a22 || a21 <= a11 && a22 >= a12) {
            output1++
        }

        if(!(a12 < a21 || a22 < a11 )){output2 ++}

    }

    println(output1)
    println(output2)
}