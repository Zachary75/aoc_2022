import java.io.File
import java.lang.RuntimeException

fun main(args: Array<String>) {
    val preinput = File("C:\\Users\\zacha\\IdeaProjects\\AdventOfCode_2022\\src\\main\\resources\\input").readText()
    val input = preinput.trim().split("\r\n")
    var firstHalf = ""
    var secondHalf = ""
    var sum = 0
    var simString = ""
    var prio ="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

    //part 1
    for(s in input){
        val sus = s.trim()
        firstHalf = sus.substring(0,sus.length/2)
        secondHalf= sus.substring(sus.length/2)
        var hasDone = false
        for(j in firstHalf){ 1
            for(k in secondHalf){2
                if(j==k){
                    var index = prio.indexOf(j)
                    if (index != -1){
                      if (!hasDone){
                        sum += index+1
                      hasDone = true }
                    }
                    else{throw RuntimeException()}

                }
            }
        }
    }


    println(sum)
    println(simString)
    println("ignore above")
    
    //part 2
    var newSum = 0

    for((a,b,c) in input.asSequence().windowed(3,3)){
        println("i'm working :)")
        var hasDone = false
        for(x in a.trim()){
            for(y in b.trim()){
                for(z in c.trim()){
                    if(x==y && y==z){
                        var index =prio.indexOf(x)
                        if(index > -1){
                            if (!hasDone){
                                newSum += index+1
                                hasDone = true }
                        }
                        else{throw RuntimeException()}
                    }
                }
            }
        }

    }
    println(newSum)
}